const Stats = (props) => {
    let Forum = props.Forum;
    let stats = [];
    let stat = 0;

    for(let cat of Forum.categories){
        for(let ind in cat.topicList)
        {
            stats.push(
                <tr key={"stats"+stat} className={"col"+stat%2}>
                    <td>
                        {cat.topicList[ind].listPosts[ind].author}
                    </td>
                    <td>
                        {cat.topicList[ind].listPosts[ind].replies}
                    </td>
                </tr>
            )
            stat++;
        }
    }

    return ( 
        <section className="table" id="stats">
            <h2 className="title" id="statsTitle">Stats Posts</h2>
                <table>
                    <tbody>
                        {stats}
                    </tbody>
                </table>
        </section>
     );
}
 
export default Stats;