const Ranked = (props) => {
    let Forum = props.Forum;
    let titleArr =[];
    let rank = 1;
    let ranker = 0;

    for(let cat of Forum.categories) {
        for(let ind in cat.topicList) {
            titleArr.push(
                <tr key={"rank"+rank} className={"col"+ranker%2}>
                    <td>
                        {cat.topicList[ind].topic_title}
                    </td>
                    <td>
                        {cat.topicList[ind].nberPost}
                    </td>
                    <td>
                        {cat.topicList[ind].status}
                    </td>
                </tr>
            )
            rank++;
            ranker++;
        }
    }

    return ( 
        <section className="table" id="ranked">
            <h2 className="title" id="rankedTitle">Ranked Topics</h2>
            <table>
                <thead>
                    <tr>
                        <th>title</th>
                        <th>post_nb</th>
                        <th>stat</th>
                    </tr>
                </thead>
                <tbody>
                    {titleArr}
                </tbody>
            </table>
        </section>
    );
}
 
export default Ranked;