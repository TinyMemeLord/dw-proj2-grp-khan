import { useState, useEffect } from "react";
import trash from '../Images/trash-alt-regular.svg';
import likeImg from '../Images/thumbs-up-regular.svg';
import dislike from '../Images/thumbs-down-regular.svg';

const MainBody = (props) => {
    let currentCategory = props.Category;
    let topic = props.Topic;
    let Allposts = [];

    const [check, setCheck] = useState(0);
    const [user, setUser] = useState("");
    
    useEffect( () => {
        document.getElementById("searchbar").addEventListener("keyup", filterSearch);
        setUser(document.getElementById("searchbar").value);
    });

    function filterSearch() {
        setUser(document.getElementById("searchbar").value);
        console.log(user);
        Allposts = [];
        if(document.getElementById("searchbar").value.length == 0) {
            buildPosts(props.Forum.categories);
            setCheck(0);
        } else {
            let all = props.Forum.categories;
            let check;
            let newArr = all.reduce(function(accum, category) {
                for(let elem of category.topicList) {
                    for(let post of elem.listPosts) {
                        check = post.author;
                        if(check.includes(user)) {
                            accum.push(post);
                        }
                        check = post.text;
                        if(check.includes(user)) {
                            accum.push(post);
                        }
                    }
                }
                return accum;
            }, []);   
            console.log(newArr) 
            for(let post of newArr) {
                Allposts.push(
                    <section id={post.topic_id} className="singlePost">
                        <section className="row">
                            <span>{post.text}</span>
                            <img onClick={ () => {
                                handleLikeAdd(post.topic_id , 1 , post)
                            }} className="like" src={likeImg} alt="like button" width="16px" height="16px"/>
                            <img onClick={ () => {
                                handleLikeRemove(post.topic_id , 1 , post)
                            } } 
                                className="dislike" src={dislike} alt="dislike button" width="16px" height="16px"/>
                        </section>
                        <section className="row">
                            <span>by: {post.author}</span>
                            <span>{post.date}</span>
                            <span>likes: {post.likes}</span>
                            <img className="trash" src={trash} alt="trash icon" width="16px" height="16px" onClick={()=>{
                                handlePostDelete(post.topic_id)
                            }}/>
                        </section>
                    </section>
                )
            }
            setCheck(Allposts);
            buildPosts(props.Forum.categories);
        }
    }

    function handlePostDelete(taskId) {
        try {
            let element = document.getElementById(taskId);
            while(element.firstChild) {
                element.removeChild(element.firstChild);
            }
        } catch {
            console.log('failed');
        }
    }

    function buildPosts(categories) {
        Allposts = [];
        if(check === 0) {
            for(let cat of categories) {
                if(cat.name === currentCategory) {
                    for(let top of cat.topicList) {
                        if(topic === top.topic_title) {
                            for(let post of top.listPosts) {
                                Allposts.push(
                                    <section key={post.topic_id} id={post.topic_id} className="singlePost">
                                        <section className="row">
                                            <span>{post.text}</span>
                                            <img onClick={ () => {
                                                handleLikeAdd(post.topic_id , 1 , post)
                                            }} className="like" src={likeImg} alt="like button" width="16px" height="16px"/>
                                            <img onClick={ () => {
                                                handleLikeRemove(post.topic_id , 1 , post)
                                            } } 
                                                className="dislike" src={dislike} alt="dislike button" width="16px" height="16px"/>
                                        </section>
                                        <section className="row">
                                            <span>by: {post.author}</span>
                                            <span>{post.date}</span>
                                            <span>likes: {post.likes}</span>
                                            <img className="trash" src={trash} alt="trash icon" width="16px" height="16px" onClick={()=>{
                                                handlePostDelete(post.topic_id)
                                            }}/>
                                        </section>
                                    </section>
                                )
                            }
                        }
                    }
                }
            }
        } else {
            Allposts = check;
        }
        return(
            <section id="listPosts">{Allposts}</section>
        )
    }

    function handleLikeAdd(l , num , postObj) {
        let likeElement = document.getElementById(l);
        let likeAmount = likeElement.childNodes[1].childNodes[2];
        postObj.likes = postObj.likes + num;
        likeAmount.textContent = `likes: ${postObj.likes}`
    }

    function handleLikeRemove(l , num , postObj) {
        let likeElement = document.getElementById(l);
        let likeAmount = likeElement.childNodes[1].childNodes[2];
        postObj.likes = postObj.likes - num;
        likeAmount.textContent = `likes: ${postObj.likes}`
    }

    return(
        buildPosts(props.Forum.categories)
    )
}

export default MainBody;