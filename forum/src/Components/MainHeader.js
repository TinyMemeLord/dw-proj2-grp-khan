import React, {useEffect, useState} from 'react';
import MainBody from './MainBody';

const MainHeader = (props) =>{
    const [catName, setCatName] = useState(props.Forum.categories[0].name);
    const [topic, setTopic] = useState(props.Forum.categories[0].topicList[0].topic_title);

    useEffect(()=>{
        setCatName(document.getElementById("categories").value);
        setTopic(document.getElementById("topics").value);
    });

    function buildCategories(categories){
        let selects = [];
        for(let cat of categories)
        {
            selects.push(
                <option key = {cat.name} id = {cat.name}>{cat.name}</option>
            )
        } 
        return(
            <select id="categories" onChange={() => {
                setCatName(document.getElementById("categories").value);
                setTopic(document.getElementById("topics").value);
            }
            }>
                {selects}
            </select>
        )
    }

    function buildTopics(categories){
        let topics = [];
        for(let cat of categories)
        {
            if(cat.name === catName)
            {
                for(let ind in cat.topicList)
                {
                    topics.push(
                        <option key = {cat.topicList[ind].topic_title} id = {cat.topicList[ind].topic_title}>{cat.topicList[ind].topic_title}</option>
                    )
                }
            }
        }
        return(
            <select id="topics" onChange={() => {
                setTopic(document.getElementById("topics").value);
            }
            }>
                {topics}
            </select>
        )
    }

    return(
        <div id="mainSectionBody">
            <section id="selections">
                <div id="category">
                    <p>Category</p>
                    {buildCategories(props.Forum.categories)}
                </div>
                <div id="topic">
                    <p>Topic</p>
                    {buildTopics(props.Forum.categories)}
                </div>
            </section>
            <section id="posts">
                <MainBody Forum = {props.Forum} Category = {catName} Topic = {topic}/>
            </section>
        </div>
    )
}

export default MainHeader;