const Recent = (props) => {
    let Forum = props.Forum;
    let author = [];
    let recent = 0;

    for(let cat of Forum.categories) {
        for(let ind in cat.topicList) {
            author.push(
                <tr key={"recent"+recent} className={"col"+recent%2}>
                    <td>
                        {cat.topicList[ind].listPosts[ind].author}
                    </td>
                    <td>
                        {cat.topicList[ind].listPosts[ind].replies}
                    </td>
                    <td>
                        {cat.topicList[ind].listPosts[ind].date}
                    </td>
                </tr>
            )
            recent++;
    }
}
    return (
        <section className="table" id="recent">
        <h2 className="title" id="recentTitle">Recent Posts</h2>
            <table>
                <thead>
                    <tr>
                        <th>author</th>
                        <th>rank</th>
                        <th>when</th>
                    </tr>
                </thead>
                <tbody>
                    {author}
                </tbody>
            </table>
        </section>
      );
}
 
export default Recent;