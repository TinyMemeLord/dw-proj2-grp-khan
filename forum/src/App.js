import Header from './Header.js';
import Footer from './Footer.js';
import Main from './Main.js';
import './App.css';
import './styles.css';
import Forum from './forum.json';

function App() {
  return (
    <div className="App">
      <Header Forum={Forum}/>
      <Main />
      <Footer />
    </div>
  );
}

export default App;
