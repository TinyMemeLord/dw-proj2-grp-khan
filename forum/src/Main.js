//import LeftColumn from './MainComponents/LeftColumn';
import MainColumn from './MainComponents/MainColumn';
import RightColumn from './MainComponents/RightColumn';
import Forum from './forum.json';

const Main = () =>{
    return(
        <section className="mainSection">
            <MainColumn Forum = {Forum}/>
            <RightColumn Forum = {Forum}/>
        </section>
    )
}

export default Main;