import Ranked from '../Components/Ranked';
import Recent from '../Components/Recent';
import Stats from '../Components/Stats';
const RightColumn = (props) =>{
    return(
        <section id="right">
            <Ranked Forum = {props.Forum}/>
            <Recent Forum = {props.Forum}/>
            <Stats Forum = {props.Forum}/>
        </section>
    )
}

export default RightColumn;