import MainHeader from '../Components/MainHeader.js';

const MainColumn = (props) =>{
    return(
        <section id="main">
            <MainHeader Forum = {props.Forum}/>
        </section>
    )
}

export default MainColumn;