const Footer = () => {
    return ( 
        <footer>
            <h2>Project 2 - Using React</h2>
            <p>Alexander Efstathakis (2043441), Mohammad Khan (2038700), Shay Alex Lelichev (2043812)</p>
        </footer>
     );
}
 
export default Footer;